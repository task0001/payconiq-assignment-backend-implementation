--liquibase formatted sql

CREATE SEQUENCE stock_seq AS  int  START  WITH 4  INCREMENT  BY  1;

--changeset vitor:create-stocks-table
CREATE TABLE stocks (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(40) DEFAULT NULL,
  price DECIMAL(5,2) DEFAULT NULL,
  created_date datetime DEFAULT NULL,
  modified_date datetime DEFAULT NULL,
  locked bool DEFAULT NULL,
  PRIMARY KEY (id)
);
--rollback DROP TABLE stocks;

--changeset vitor:seed-initial-data
SET FOREIGN_KEY_CHECKS=0;
truncate table stocks;

INSERT INTO stocks (id, name, price, created_date, modified_date, locked) VALUES ('1', 'AAA', '100.00', '2021-08-18 11:00:00.605000', '2021-08-18 11:00:00.605000', '0');
INSERT INTO stocks (id, name, price, created_date, modified_date, locked) VALUES ('2', 'BBB', '200.00', '2021-08-18 12:00:00.605000', '2021-08-18 12:00:00.605000', '0');
INSERT INTO stocks (id, name, price, created_date, modified_date, locked) VALUES ('3', 'CCC', '300.00', '2021-08-18 13:00:00.605000', '2021-08-18 13:00:00.605000', '0');
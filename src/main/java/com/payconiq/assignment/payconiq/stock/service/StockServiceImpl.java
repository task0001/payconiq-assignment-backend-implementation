package com.payconiq.assignment.payconiq.stock.service;

import com.payconiq.assignment.payconiq.stock.event.UnlockStockEvent;
import com.payconiq.assignment.payconiq.stock.exception.LockedStockException;
import com.payconiq.assignment.payconiq.stock.exception.StockNotFoundException;
import com.payconiq.assignment.payconiq.stock.model.Stock;
import com.payconiq.assignment.payconiq.stock.repository.AmountRepository;
import com.payconiq.assignment.payconiq.stock.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class StockServiceImpl implements StockService{
  private final static double variation = 0.15; // TODO get from config

  private final StockRepository stockRepository;
  private final ApplicationEventPublisher publisher;


  public Page<Stock> getPaginatedStocks(int page, int size) {
    return stockRepository.findAll(PageRequest.of(page, size));
  }

  @Override
  public List<Stock> getAll() {
    return stockRepository.findAll();
  }

  @Override
  public Stock getStock(Long id) {
    return stockRepository.findById(id)
            .orElseThrow(() -> new StockNotFoundException(id));
  }

  @Override
  public Stock saveStock(Stock stock) {
    stock.setLocked(false);
    return stockRepository.save(stock);
  }

  @Override
  public Stock update(Long id, Stock stock) {
    var currentStock = stockRepository
            .findById(id)
            .orElseThrow(() -> new StockNotFoundException(id));

    if(!currentStock.isLocked()){
      if (isValidPrice(stock, currentStock)){
        return stockRepository.save(stock);
      } else {
        return lockStock(currentStock);
      }
    } else {
      throw new LockedStockException(id);
    }
  }

  @Override
  public void deleteStocks(Long id) {
    var currentStock = stockRepository
            .findById(id)
            .orElseThrow(() -> new StockNotFoundException(id));
    if (!currentStock.isLocked()){
      stockRepository.deleteById(id);
    }
  }

  @Override
  public void unlockStock(Long id) {
    var stock = stockRepository
            .findById(id)
            .orElseThrow(() -> new StockNotFoundException(id));
    stock.setLocked(false);
    stockRepository.save(stock);
  }

  private boolean isValidPrice(final Stock stock, final Stock currentStock){
    var maxPrice = currentStock.getPrice() * (1 + variation);
    var minPrice = currentStock.getPrice() * (1 - variation);
    if (stock.getPrice() > maxPrice || stock.getPrice() < minPrice){
      return false;
    }
    return true;
  }

  private Stock lockStock(final Stock stock){
    stock.setLocked(true);
    var updatedStock = stockRepository.save(stock);
    publisher.publishEvent(new UnlockStockEvent(this, updatedStock, TimeUnit.MINUTES));
    return updatedStock;
  }
}

package com.payconiq.assignment.payconiq.stock.repository;

import com.payconiq.assignment.payconiq.stock.model.Amount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmountRepository extends JpaRepository<Amount, Long> {
}
package com.payconiq.assignment.payconiq.stock.event;

import com.payconiq.assignment.payconiq.stock.model.Stock;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.concurrent.TimeUnit;

@Getter
public class UnlockStockEvent extends ApplicationEvent {
  private final Stock stock;
  private final TimeUnit timeUnit;

  public UnlockStockEvent(final Object source, final Stock stock, final TimeUnit timeUnit) {
    super(source);
    this.stock = stock;
    this.timeUnit = timeUnit;
  }
}

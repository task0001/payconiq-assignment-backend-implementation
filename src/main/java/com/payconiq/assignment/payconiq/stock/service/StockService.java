package com.payconiq.assignment.payconiq.stock.service;

import com.payconiq.assignment.payconiq.stock.model.Stock;
import org.springframework.data.domain.Page;

import java.util.List;

public interface StockService {

  Page<Stock> getPaginatedStocks(int page, int size);
  List<Stock> getAll();
  Stock getStock(Long id);
  Stock saveStock(Stock stock);
  Stock update(Long id, Stock stock);
  void deleteStocks(Long id);
  void unlockStock(Long id);
}

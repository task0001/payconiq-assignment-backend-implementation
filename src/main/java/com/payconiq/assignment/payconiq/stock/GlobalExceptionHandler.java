package com.payconiq.assignment.payconiq.stock;

import com.payconiq.assignment.payconiq.stock.exception.LockedStockException;
import com.payconiq.assignment.payconiq.stock.exception.StockNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
  /**
   * Handler for NotFoundException
   *
   * @param notFoundException Exception to react to
   * @return prepared ResponseEntity
   */
  @ExceptionHandler(StockNotFoundException.class)
  public ResponseEntity<Object> handleNotFound(StockNotFoundException notFoundException) {
    return new ResponseEntity<>(buildErrorResponse(notFoundException), HttpStatus.NOT_FOUND);
  }

  /**
   * Handler for LockedStockException
   *
   * @param lockedStockException Exception to react to
   * @return prepared ResponseEntity
   */
  @ExceptionHandler(LockedStockException.class)
  public ResponseEntity<Object> handleNotFound(LockedStockException lockedStockException) {
    return new ResponseEntity<>(buildErrorResponse(lockedStockException), HttpStatus.NOT_ACCEPTABLE);
  }

  private Map<String, Object> buildErrorResponse(StockNotFoundException httpException) {
    Map<String, Object> errorMap = new HashMap<>();
    errorMap.put("message", httpException.getMessage());
    return errorMap;
  }

  private Map<String, Object> buildErrorResponse(LockedStockException httpException) {
    Map<String, Object> errorMap = new HashMap<>();
    errorMap.put("message", httpException.getMessage());
    return errorMap;
  }
}

package com.payconiq.assignment.payconiq.stock.exception;

public class StockNotFoundException extends RuntimeException {
  private final static String message = "Stock not found. Id: ";
  public StockNotFoundException(Long id) {
    super(message.concat(String.valueOf(id)));
  }
}

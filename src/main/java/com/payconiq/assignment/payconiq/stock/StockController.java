package com.payconiq.assignment.payconiq.stock;

import com.payconiq.assignment.payconiq.stock.model.Stock;
import com.payconiq.assignment.payconiq.stock.service.StockServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping(value = "/stock")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class StockController {
  private final StockServiceImpl stockService;

  /**
   * Returns a list of stocks.
   *
   * @return A list of {@link Stock}.
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public List<Stock> getPaginatedStocks() {
    return stockService.getAll();
  }

  @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Stock getStockById(@PathVariable Long id){
    return stockService.getStock(id);
  }

  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public Stock saveStock(@RequestBody Stock stock){
    return stockService.saveStock(stock);
  }

  @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Stock updateStock(@PathVariable Long id, @RequestBody Stock stock){
    return stockService.update(id, stock);
  }

  @DeleteMapping(value = "{id}")
  public ResponseEntity<Void> deleteById(@PathVariable Long id){
    stockService.deleteStocks(id);
    return ResponseEntity.ok().build();
  }
}

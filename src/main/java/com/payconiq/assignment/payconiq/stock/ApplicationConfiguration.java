package com.payconiq.assignment.payconiq.stock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
public class ApplicationConfiguration {

  @Bean
  protected  ScheduledExecutorService scheduledExecutorService(){
    return Executors.newSingleThreadScheduledExecutor();
  }
}

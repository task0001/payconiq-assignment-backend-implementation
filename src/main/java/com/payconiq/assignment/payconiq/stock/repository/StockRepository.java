package com.payconiq.assignment.payconiq.stock.repository;

import com.payconiq.assignment.payconiq.stock.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, Long> {
}

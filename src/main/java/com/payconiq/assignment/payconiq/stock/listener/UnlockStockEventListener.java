package com.payconiq.assignment.payconiq.stock.listener;

import com.payconiq.assignment.payconiq.stock.event.UnlockStockEvent;
import com.payconiq.assignment.payconiq.stock.service.StockService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@RequiredArgsConstructor
public class UnlockStockEventListener implements ApplicationListener<UnlockStockEvent> {

  @Value("${lock.time:5}")
  private int waitingUnits;

  private final StockService stockService;
  private final ScheduledExecutorService executorService;

  @Override
  public void onApplicationEvent(UnlockStockEvent unlockStockEvent) {
    executorService.schedule(() -> stockService.unlockStock(unlockStockEvent.getStock().getId()), waitingUnits, unlockStockEvent.getTimeUnit());

  }
}

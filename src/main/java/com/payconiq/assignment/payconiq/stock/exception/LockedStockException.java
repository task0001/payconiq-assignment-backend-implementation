package com.payconiq.assignment.payconiq.stock.exception;

public class LockedStockException extends RuntimeException {
  private final static String message = "Stock is blocked. Id: ";
  public LockedStockException(Long id) {
    super(message.concat(String.valueOf(id)));
  }
}
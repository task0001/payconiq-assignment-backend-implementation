package com.payconiq.assignment.payconiq.stock.listener;

import com.payconiq.assignment.payconiq.stock.event.UnlockStockEvent;
import com.payconiq.assignment.payconiq.stock.service.StockService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.payconiq.assignment.payconiq.stock.util.StockUtils.createStock;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UnlockStockEventListenerTest {

  @Mock
  private StockService stockService;

  private ScheduledExecutorService executorService;
  private UnlockStockEventListener unlockStockEventListener;

  @BeforeEach
  public void setUp(){
    executorService = Executors.newSingleThreadScheduledExecutor();
    unlockStockEventListener = new UnlockStockEventListener(stockService, executorService);

    ReflectionTestUtils.setField(unlockStockEventListener, "waitingUnits", 3);
  }

  @Test
  public void  given_a_new_unlockStockEvent_then_listener_should_be_called() throws InterruptedException {
    var expectedStock = createStock();
    var expectedEvent = new UnlockStockEvent(this, expectedStock, TimeUnit.SECONDS);

    doNothing().when(stockService).unlockStock(expectedStock.getId());

    unlockStockEventListener.onApplicationEvent(expectedEvent);

    Thread.sleep(4000);
    verify(stockService, times(1)).unlockStock(expectedStock.getId());

  }
}

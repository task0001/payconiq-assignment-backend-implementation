package com.payconiq.assignment.payconiq.stock.service;

import com.payconiq.assignment.payconiq.stock.exception.StockNotFoundException;
import com.payconiq.assignment.payconiq.stock.repository.StockRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;

import static com.payconiq.assignment.payconiq.stock.util.StockUtils.createStock;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class StockServiceImplTest {

  @Mock
  private StockRepository stockRepository;

  @Mock
  private ApplicationEventPublisher publisher;

  private StockService stockService;

  @BeforeEach
  void setUp(){
    stockService = new StockServiceImpl(stockRepository, publisher);
  }

  @Test
  void HavingStockAndPageAndSize_When_GetPaginatedStocks_Then_PageOfStockReturned() {
    var pageNumber = 1;
    var itemsPerPage = 10;
    var expectedPage = new PageImpl<>(List.of(createStock()));
    var pageRequest = PageRequest.of(pageNumber, itemsPerPage);

    when(stockRepository.findAll(pageRequest)).thenReturn(expectedPage);

    var result = stockService.getPaginatedStocks(pageNumber, itemsPerPage);

    verify(stockRepository, times(1)).findAll(pageRequest);
    assertEquals(expectedPage, result);
  }

  @Test
  void HavingStockAndId_When_GetStock_Then_StockIsReturned() {
    var expectedStock = createStock();

    when(stockRepository.findById(expectedStock.getId())).thenReturn(Optional.of(expectedStock));

    var result = stockService.getStock(expectedStock.getId());

    verify(stockRepository, times(1)).findById(expectedStock.getId());
    assertEquals(expectedStock, result);
  }

  @Test
  void NotHavingStockAndId_When_GetStock_Then_ThrowStockNotFoundException() {
    when(stockRepository.findById(anyLong())).thenReturn(Optional.empty());

    assertThrows(StockNotFoundException.class,
            () -> stockService.getStock(123L));

  }

  @Test
  void GivenAStock_When_SaveStock_Then_StockedReturned() {
    var expectedStock = createStock();
    when(stockRepository.save(expectedStock)).thenReturn(expectedStock);

    var result = stockService.saveStock(expectedStock);

    verify(stockRepository, times(1)).save(expectedStock);
    assertEquals(expectedStock.getName(), result.getName());
    assertFalse(result.isLocked());

  }
}

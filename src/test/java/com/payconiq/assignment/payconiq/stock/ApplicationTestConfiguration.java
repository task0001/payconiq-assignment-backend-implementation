package com.payconiq.assignment.payconiq.stock;

import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class ApplicationTestConfiguration {

  @Bean
  EmbeddedMongoProperties embeddedProperties() {
    var embeddedMongoProperties = new EmbeddedMongoProperties();
    embeddedMongoProperties.setVersion("4.0.2");
    return embeddedMongoProperties;
  }
}

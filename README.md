## Run the application

1 - Build the application
```
mvn clean install
```

2 - Run Spring Boot from maven
```
mvn spring-boot:run
```

## Run the application from docker

1 - Build the application
```
docker-compose -f ./docker-compose.yml up --build -d
```

## How to change for a dislodged data-base
Changing this you will be able to run from the docker or manually configure your data-base.

Here is an example of how to change it and run the application using docker.

1 - remove from the pom.xml the dependency
```
    <dependency>
        <groupId>com.h2database</groupId>
        <artifactId>h2</artifactId>
        <scope>runtime</scope>
    </dependency>
```

2 - change the application.yml with the content
```
server:
  port: ${SERVER_PORT:5003}
  servlet:
    context-path: /payconiq

spring:
  main:
    allow-bean-definition-overriding: true
  application:
    name: stock-assignment
  datasource:
    url: jdbc:mysql://${DB_HOST:host.docker.internal}:${DB_PORT:3306}/${DB_DATABASE:stock}
    username: user
    password: user
    driver-class-name: com.mysql.cj.jdbc.Driver
  jpa:
    show-sql: false
  liquibase:
    changelog: classpath:/db/changelog.xml

lock:
  time: 5
```

3 - Update the docker-compose.yml file to the following
```
version: '3.7'
services:
  api:
    container_name: stock_service
    environment:
      DB_HOST: db
    networks:
      - task
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - '5003:5003'
    depends_on:
      - db
    links:
      - db
    restart: always

  db:
    container_name: stock_db
    image: mysql:5.7
    command: --default-authentication-plugin=mysql_native_password  --character-set-server=utf8mb4
    environment:
      MYSQL_DATABASE: stock
      MYSQL_USER: user
      MYSQL_PASSWORD: user
      MYSQL_ROOT_PASSWORD: root
    networks:
      - task
    ports:
      - '3306:3306'

networks:
  task:
    name: task-network
```

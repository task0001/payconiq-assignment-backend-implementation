FROM  azul/zulu-openjdk:11 as build

WORKDIR /workspace/app

RUN apt-get update || true && apt-get -y install unzip wget curl vim \
    && wget https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.3.9/apache-maven-3.3.9-bin.tar.gz \
    && tar xzf apache-maven-3.3.9-bin.tar.gz

COPY pom.xml .
# COPY settings.xml .

RUN apache-maven-3.3.9/bin/mvn -B -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=info dependency:go-offline

COPY src src
COPY lombok.config .

RUN apache-maven-3.3.9/bin/mvn install -DskipTests
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

### execution stage ###
FROM  azul/zulu-openjdk:11
VOLUME /tmp
ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT exec java -Xms256m -Xmx512m -XX:+UseStringDeduplication -noverify -cp app:app/lib/* com.payconiq.assignment.payconiq.stock.StockApplication